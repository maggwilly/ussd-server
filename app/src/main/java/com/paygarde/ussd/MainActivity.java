package com.paygarde.ussd;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.paygarde.ussd.service.Globals;
import com.paygarde.ussd.service.RestartBroadcastReceiver;

import org.androidannotations.annotations.EActivity;

import java.util.Objects;

import butterknife.ButterKnife;

@EActivity
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "AppCompatActivity";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);

        IntentFilter filter = new IntentFilter();
        filter.addAction(Globals.ACTION_FOO);
        LocalBroadcastManager bm = LocalBroadcastManager.getInstance(this);
        bm.registerReceiver(mBroadcastReceiver, filter);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }


    public void onStartService() {
        RestartBroadcastReceiver.scheduleJob(getApplicationContext());
    }

    @Override
    protected void onResume() {
        super.onResume();
      this.onStartService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Objects.equals(intent.getAction(), Globals.ACTION_FOO)) {
                final boolean isConnected = intent.getBooleanExtra(Globals.EXTRA_PARAM_A, false);
                if(isConnected)
                  Objects.requireNonNull(getSupportActionBar()).setSubtitle("Connected");
                else {
                    Objects.requireNonNull(getSupportActionBar()).setSubtitle("Disconnected");
                }
            }
        }
    };
}