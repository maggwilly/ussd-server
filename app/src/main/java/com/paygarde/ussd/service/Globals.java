package com.paygarde.ussd.service;

public class Globals {
    public static final String RESTART_INTENT = "com.paygarde.ussd.service";
    public static final String ACTION_FOO = "com.gahlot.neverendingservice.FOO";
    public static final String EXTRA_PARAM_A = "com.gahlot.neverendingservice.PARAM_A";
}