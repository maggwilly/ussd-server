package com.paygarde.ussd.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.widget.Toast;

import androidx.preference.PreferenceManager;

import com.paygarde.ussd.model.MessageData;
import com.paygarde.ussd.model.MessageType;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EReceiver;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.stream.Collectors;

@EReceiver
public class SMSReceiver extends BroadcastReceiver{
    private static final String TAG = SMSReceiver.class.getSimpleName();
    public static final String pdu_type = "pdus";
    @Bean
    SocketEmitter socketEmitter;
    private SharedPreferences prefs;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle= intent.getExtras();
        String format= bundle.getString("format");
        Object[] pdus= (Object[]) bundle.get(pdu_type);
        if (pdus != null) {
            prefs = PreferenceManager.getDefaultSharedPreferences(context);
            String strMessage = Arrays.stream(pdus).map(o -> SmsMessage.createFromPdu((byte[]) o, format).getMessageBody()).collect(Collectors.joining());
            Toast.makeText(context, strMessage, Toast.LENGTH_LONG).show();
            String smsOriginRegex = prefs.getString("smsOriginRegex", StringUtils.EMPTY);
            String originatingAddress = Arrays.stream(pdus).findFirst().map(o -> SmsMessage.createFromPdu((byte[]) o, format).getOriginatingAddress()).orElse(StringUtils.EMPTY);
            if (originatingAddress.matches(smsOriginRegex)) {
                    MessageData messageData = MessageData.Builder()
                            .setMessageType(MessageType.ASYNC_RESPONSE)
                            .setMessage(strMessage);
                    socketEmitter.emit("dataEvent", messageData);

            }
        }
    }
}
