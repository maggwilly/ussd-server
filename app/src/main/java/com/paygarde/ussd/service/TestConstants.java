package com.paygarde.ussd.service;

public class TestConstants {
    public static  String BALANCE_RESPONSE_MESSAGE_SYNC = "Le solde de votre compte est de 6572.8 FCFA. Beneficiez de 100% de bonus en rechargeant via Orange Money dans My Orange.";
    public static  String REQUEST_RESPONSE_MESSAGE_SYNC = "Retrait effectue. Le client doit confirmer le retrait en entrant son code secret. Vous recevrez ensuite un sms de confirmation. Merci d avoir utilise le service Orange Money";

}
