package com.paygarde.ussd.service;

import android.content.Context;

import com.google.gson.Gson;
import com.paygarde.ussd.UssdApplication;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.App;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.json.JSONException;
import org.json.JSONObject;

import io.socket.client.Socket;

@EBean(scope = EBean.Scope.Singleton)
public class SocketEmitter {
    static final String TAG = "SocketEmitter";
    @RootContext
    Context context;
    @App
    UssdApplication app;
    private Socket mSocket;
    Gson gson = new Gson();
    @AfterInject
    public void afterInjection() {
        mSocket = app.getSocket();
    }
    public void emit(String event, Object data) {
        try {
            JSONObject mJSONObject = new JSONObject(gson.toJson(data));
            mSocket.emit(event, mJSONObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
