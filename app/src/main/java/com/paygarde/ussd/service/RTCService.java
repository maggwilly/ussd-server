package com.paygarde.ussd.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.provider.Telephony;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paygarde.ussd.MainActivity;
import com.paygarde.ussd.R;
import com.paygarde.ussd.UssdApplication;
import com.paygarde.ussd.dto.PaymentProcess;
import com.paygarde.ussd.model.BalanceRequest;
import com.paygarde.ussd.model.ClientDetail;
import com.paygarde.ussd.model.MessageData;
import com.paygarde.ussd.model.MessageType;
import com.paygarde.ussd.model.ProcessStatus;
import com.paygarde.ussd.model.UssdCallBack;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EService;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.math.NumberUtils;

import java.text.DateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;

@EService
public class RTCService extends Service {
    static final String TAG = "RTCService";
    public static final String NOTIFICATION = "com.paygarde.ussd.service";
    public static final String DATA = "data";
    public static final String RESULT = "result";
    public static final String TYPE = "type";
    public static final String TYPE_MESSAGE = "message";
    public static final String TYPE_DATA = "data";
    private Timer timer;
    private TimerTask timerTask;
    private Socket mSocket;
    @Bean
    UssdService ussdService;
    @App
    UssdApplication application;
    @Bean
    SocketEmitter socketEmitter;
    @Nullable
    private int retryTime = 0;
    private SharedPreferences prefs;
    final Gson gson = new GsonBuilder().setDateFormat(DateFormat.FULL, DateFormat.DEFAULT).create();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);
        String message = "Ussd service running";

        Notification notification = new NotificationCompat.Builder(this, UssdApplication.CHANNEL_ID)
                .setAutoCancel(false)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(Notification.PRIORITY_MAX)
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setColor(Color.parseColor("#0f9595"))
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setContentIntent(pendingIntent)
                .build();
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(UssdApplication.CHANNEL_ID, UssdApplication.CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT);
            mNotificationManager.createNotificationChannel(channel);
            new NotificationCompat.Builder(this, UssdApplication.CHANNEL_ID);
        }
        startForeground(1, notification);
        startTimer(this);
        return Service.START_STICKY;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if (BooleanUtils.isTrue(prefs.getBoolean("retry", Boolean.FALSE))) {
            String retryTimes = prefs.getString("retryTimes", "0");
            if (NumberUtils.isNumber(retryTimes))
                retryTime = Integer.parseInt(retryTimes);
        }

    }

    public void initialiseTimerTask(final Context context) {
        timerTask = new TimerTask() {
            @Override
            public void run() {
                //deleteOldSms();
                if(mSocket==null)
                    initSocket();
                if(!mSocket.connected()) {
                    mSocket.connect();
                    Log.i(TAG, "socket connexion ... ");
                }
                broadcastActionBaz(context,mSocket.connected());
            }
        };
    }

    private void initSocket() {
        mSocket = application.getSocket();
        mSocket.on(Socket.EVENT_CONNECT, onConnect)
                .on(Socket.EVENT_DISCONNECT, onDisconnect)
                .on(Socket.EVENT_CONNECT_ERROR, onConnectError)
                .on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
                .on("startProcess", onProcessData)
                .on("updates", onUpdates)
                .on("requestBalance", onRequestBalance);;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
        mSocket.off(Socket.EVENT_CONNECT, onConnect)
                .off(Socket.EVENT_DISCONNECT, onDisconnect)
                .off(Socket.EVENT_CONNECT_ERROR, onConnectError)
                .off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
                .off("startProcess", onProcessData)
                .off("updates", onUpdates)
                .off("requestBalance", onRequestBalance);
                Intent broadcastIntent = new Intent(this, RestartBroadcastReceiver.class);
                sendBroadcast(broadcastIntent);
                stoptimertask();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public Emitter.Listener onConnect = args -> {
        Log.d(TAG, "Connected");
        notify("Connected");
        Object brand = String.format("%s %s", Build.BRAND, Build.MODEL);
        ClientDetail data = new ClientDetail("brand", brand);
        socketEmitter.emit("clientDetailEvent", data);
    };

    public Emitter.Listener onDisconnect = args -> {
        notify("Disconnected");
        Log.d(TAG, "Disconnected");
    };


    public Emitter.Listener onConnectError = args -> {
        notify("Connexion errors");
        Log.e(TAG, "Connexion errors");
    };
    public Emitter.Listener onUpdates = args -> {
        final PaymentProcess[] paymentProcess = gson.fromJson(args[0].toString(), PaymentProcess[].class);
        // processListViewModel.setUpdates(Arrays.asList(paymentProcess));
        publishResults(paymentProcess[0]);
    };

    public Emitter.Listener onProcessData = args -> {
        Log.d(TAG, args[0].toString());
        final PaymentProcess paymentProcess = gson.fromJson(args[0].toString(), PaymentProcess.class);
        paymentProcess.setNew(Boolean.TRUE);
        publishResults(paymentProcess);
        final String ussd = getUssdPaymentCode(paymentProcess);

        ussdService.sentUSSDRequest(ussd, new UssdCallBack() {
            @Override
            public void onComplete(Object ussdResult, int code) {
                MessageData messageData = MessageData.Builder()
                        .setProcessId(paymentProcess.getId())
                        .setMessageType(MessageType.RESPONSE)
                        .setMessage(ussdResult)
                        .setData(code)
                        .setCode(code);
                socketEmitter.emit(paymentProcess.getId()+"_startRequestEvent", messageData);
                String log = createLog(ussdResult, code);
                paymentProcess.getLogs().add(log);
                if (code != 0) {
                    paymentProcess.setStatus(ProcessStatus.FAILED);
                    paymentProcess.getLogs().add(String.format("%s | Request failed %s", ussd, LocalDateTime.now()));
                } else
                    paymentProcess.getLogs().add(String.format("%s | Waiting confirmation sms", LocalDateTime.now()));
                publishResults(paymentProcess);
            }

            private String createLog(Object ussdResult, int code) {
                if (code == 0)
                    return String.format("%s | Ussd call successful", LocalDateTime.now());
                return String.format("%s | %s", LocalDateTime.now(), ussdResult);
            }
        });

        publishResults(paymentProcess);
    };
    private String getUssdPaymentCode(PaymentProcess processData) {
        String secretCode = prefs.getString("secretCode", "0000").trim();
        String ussdPattern = prefs.getString("ussdCodePaymentPattern", processData.getUssdPattern()).trim();
        return String.format(ussdPattern, processData.getAmount(), processData.getMsinsdn(), secretCode);
    }

    public Emitter.Listener onRequestBalance = args -> {
        Log.d(TAG, "onRequestBalance");
        final BalanceRequest request = gson.fromJson(args[0].toString(), BalanceRequest.class);
        String ussdRequest=getUssdBalanceCode(request);
       ussdService.sentUSSDRequest(ussdRequest, (ussdResult, code) -> {
           MessageData data = MessageData.Builder()
                    .setMessageType(MessageType.RESPONSE)
                    .setMessage(ussdResult)
                    .setData(code).setCode(code);
            socketEmitter.emit( "balanceStatusEvent", data);
        });
    };

    private String getUssdBalanceCode(BalanceRequest request ) {
        String secretCode = prefs.getString("secretCode", "0000").trim();
        String ussdPattern = prefs.getString("ussdCodeBalancePattern", request.getUssdPattern()).trim();
        return String.format(ussdPattern, secretCode);
    }

    private void publishResults(PaymentProcess data) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(DATA, gson.toJson(data));
        intent.putExtra(TYPE, TYPE_DATA);
        intent.putExtra(RESULT, -1);
        sendBroadcast(intent);
    }

    private void notify(String message) {
        Intent intent = new Intent(NOTIFICATION);
        intent.putExtra(DATA, message);
        intent.putExtra(TYPE, TYPE_MESSAGE);
        intent.putExtra(RESULT, -1);
        sendBroadcast(intent);
    }



    public void startTimer(Context context) {
        timer = new Timer();
        //initialize the TimerTask's job
        initialiseTimerTask(context);
        //schedule the timer, to wake up every 30 seconds
        timer.schedule(timerTask, 1000, 30000); //
    }



    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public static void broadcastActionBaz(Context context, Boolean param) {
        Intent intent = new Intent(Globals.ACTION_FOO);
        intent.putExtra(Globals.EXTRA_PARAM_A, param);
        LocalBroadcastManager bm = LocalBroadcastManager.getInstance(context);
        bm.sendBroadcast(intent);
    }

    public void deleteOldSms(){
        long date = new Date(System.currentTimeMillis() - 1 * 60 * 1000).getTime();
        if(ContextCompat.checkSelfPermission(getBaseContext(), "android.permission.READ_SMS") == PackageManager.PERMISSION_GRANTED) {
            Uri uriSMSURI = Uri.parse("content://sms/inbox");
            Cursor cur = getContentResolver().query(uriSMSURI, null, null, null, null);
            while (cur != null && cur.moveToNext()) {
                     long id = cur.getLong(0);
                         getContentResolver().delete(
                        Uri.parse("content://sms/" + id), "date>?",
                        new String[] {String.valueOf(date)});
            }

            if (cur != null) {
                cur.close();
            }
        }
    }
}
