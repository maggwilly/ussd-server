package com.paygarde.ussd.service;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;
import androidx.core.app.ActivityCompat;
import androidx.preference.PreferenceManager;

import com.paygarde.ussd.model.ProcessData;
import com.paygarde.ussd.model.UssdCallBack;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.SystemService;

@EBean
public class UssdService{
    static final String TAG = "UssdService";
    @RootContext
    Context context;
    @SystemService
    TelephonyManager telephonyManager;

    public void sentUSSDRequest(final String ussd, final UssdCallBack callBack){
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, "PackageManager.PERMISSION_GRANTED", Toast.LENGTH_LONG).show();
            callBack.onComplete("Device client not prepared. Check permission",-1);
            return;
        }
       try {
        telephonyManager.sendUssdRequest(ussd, new TelephonyManager.UssdResponseCallback() {
            @Override
            public void onReceiveUssdResponse(TelephonyManager telephonyManager, String request, CharSequence response) {
                super.onReceiveUssdResponse(telephonyManager, request, response);
                StringBuilder sb = new StringBuilder(response);
                Log.e(TAG, sb.toString());
                Toast.makeText(context, sb.toString(), Toast.LENGTH_LONG).show();
                callBack.onComplete(sb.toString(),0);
            }

            @Override
            public void onReceiveUssdResponseFailed(TelephonyManager telephonyManager, String request, int failureCode) {
                super.onReceiveUssdResponseFailed(telephonyManager, request, failureCode);
                Log.e(TAG, request);
                Toast.makeText(context, request, Toast.LENGTH_LONG).show();
                callBack.onComplete(String.format("USSD request %s fails with code %s",ussd,failureCode), failureCode);
            }
        }, new Handler(Looper.getMainLooper()){
            @Override
            public void handleMessage(Message msg) {
                Log.e("ERROR","error");
                callBack.onComplete(String.format("USSD request fails %s ",msg), -1);
                Toast.makeText(context, msg.toString(), Toast.LENGTH_LONG).show();
            }
        });
       }catch (Exception exception){
           callBack.onComplete("Problem occurred. Network issue",-1);
       }
    }
}
