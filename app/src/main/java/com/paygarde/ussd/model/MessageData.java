package com.paygarde.ussd.model;

public class MessageData {
    private String processId;
    private MessageType messageType;
    private Object data;
    private Object message;
    private Object balanceMessage;
    private int code;
    public static MessageData Builder() {
        return new MessageData();
    }
    public String getProcessId() {
        return processId;
    }

    public MessageData setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public MessageData setMessageType(MessageType messageType) {
        this.messageType = messageType;
        return this;
    }

    public Object getBalanceMessage() {
        return balanceMessage;
    }

    public MessageData setBalanceMessage(Object balanceMessage) {
        this.balanceMessage = balanceMessage;
        return this;
    }

    public Object getData() {
        return data;
    }

    public MessageData setData(Object data) {
        this.data = data;
        return this;
    }

    public Object getMessage() {
        return message;
    }

    public MessageData setMessage(Object message) {
        this.message = message;
        return this;
    }

    public int getCode() {
        return code;
    }

    public MessageData setCode(int code) {
        this.code = code;
        return this;
    }

    @Override
    public String toString() {
        return "Data{" +
                "processId='" + processId + '\'' +
                ", dataType='" + messageType + '\'' +
                ", data=" + data +
                ", message='" + message + '\'' +
                '}';
    }
}
