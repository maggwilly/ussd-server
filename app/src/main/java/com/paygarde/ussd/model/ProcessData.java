package com.paygarde.ussd.model;
import java.util.ArrayList;
import java.util.List;

public class ProcessData {
    private String id;
    private String ussdPattern;
    private String msinsdn;
    private int amount;
    private int afterBalance;
    private ProcessStatus status;
    private String currentStatusDate;
    private String endAt;
    private String startedAt;
    private List<String> logs = new ArrayList<>();

    public double getAfterBalance() {
        return afterBalance;
    }

    public ProcessData setAfterBalance(int afterBalance) {
        this.afterBalance = afterBalance;
        return this;
    }

    public String getId() {
        return id;
    }

    public ProcessData setId(String id) {
        this.id = id;
        return this;
    }

    public String getEndAt() {
        return endAt;
    }

    public ProcessData setEndAt(String endAt) {
        this.endAt = endAt;
        return this;
    }

    public String getStartedAt() {
        return startedAt;
    }

    public ProcessData setStartedAt(String startedAt) {
        this.startedAt = startedAt;
        return this;
    }

    public List<String> getLogs() {
        return logs==null?new ArrayList<>():logs;
    }

    public ProcessData setLogs(List<String> logs) {
        this.logs = logs;
        return this;
    }

    public ProcessStatus getStatus() {
        return status;
    }

    public ProcessData setStatus(ProcessStatus status) {
        this.status = status;
        return this;
    }

    public String getCurrentStatusDate() {
        return currentStatusDate;
    }

    public ProcessData setCurrentStatusDate(String currentStatusDate) {
        this.currentStatusDate = currentStatusDate;
        return this;
    }

    public String getUssdPattern() {
        return ussdPattern;
    }

    public ProcessData setUssdPattern(String ussdPattern) {
        this.ussdPattern = ussdPattern;
        return this;
    }

    public String getMsinsdn() {
        return msinsdn;
    }

    public ProcessData setMsinsdn(String msinsdn) {
        this.msinsdn = msinsdn;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public ProcessData setAmount(int amount) {
        this.amount = amount;
        return this;
    }
}
