package com.paygarde.ussd.model;

public class BalanceRequest {
    private String processId;
    private String ussdPattern;
    private String providerId;

    public String getUssdPattern() {
        return ussdPattern;
    }

    public BalanceRequest setUssdPattern(String ussdPattern) {
        this.ussdPattern = ussdPattern;
        return this;
    }

    public String getProcessId() {
        return processId;
    }

    public BalanceRequest setProcessId(String processId) {
        this.processId = processId;
        return this;
    }

    public String getProviderId() {
        return providerId;
    }

    public BalanceRequest setProviderId(String providerId) {
        this.providerId = providerId;
        return this;
    }
}
