package com.paygarde.ussd.model;

public interface UssdCallBack {
    void onComplete(Object ussdResult, int code);
}
