package com.paygarde.ussd.model;

public class ClientDetail {
    private String clientId;
    private String key;
    private Object value;

    public ClientDetail(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public String getClientId() {
        return clientId;
    }

    public ClientDetail setClientId(String clientId) {
        this.clientId = clientId;
        return this;
    }

    public String getKey() {
        return key;
    }

    public ClientDetail setKey(String key) {
        this.key = key;
        return this;
    }

    public Object getValue() {
        return value;
    }

    public ClientDetail setValue(Object value) {
        this.value = value;
        return this;
    }

    @Override
    public String toString() {
        return "{" +
                "clientId='" + clientId + '\'' +
                ", key='" + key + '\'' +
                ", value=" + value +
                '}';
    }
}
