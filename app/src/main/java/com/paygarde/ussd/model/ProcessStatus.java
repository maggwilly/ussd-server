package com.paygarde.ussd.model;

public enum ProcessStatus {
    PROCESSING,
    WAITING_CHECK,
    FAILED,
    COMPLETED,
    SUCCESS,
    CHECK
}
