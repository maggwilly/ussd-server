package com.paygarde.ussd.model;

public enum MessageType {
     RESPONSE,
    ASYNC_RESPONSE,
    ERROR
}
