package com.paygarde.ussd.model;

public class TestData {
   public static  String confirmationMessage = "Retrait d'argent reussi par le 694210212 avec le Code : 109768. Informations detaillees : Montant: 400 FCFA, Frais: 175 FCFA, No de transaction CO210114.1941.C18289, montant net debite 6175 FCFA, Nouveau solde: 397.8 FCFA.";
   public static  String soldeMessage = "Le solde de votre compte est de 6572.8FCFA. Beneficiez de 100% de bonus en rechargeant via Orange Money dans My Orange.";
   public static  String transactionalMessage = "Retrait effectue. Le client doit confirmer le retrait en entrant son code secret. Vous recevrez ensuite un sms de confirmation. Merci d avoir utilise le service Orange Money";

}
