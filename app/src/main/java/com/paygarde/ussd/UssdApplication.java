package com.paygarde.ussd;
import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;

import androidx.preference.PreferenceManager;
import org.androidannotations.annotations.EApplication;
import org.apache.commons.lang3.StringUtils;
import java.net.URISyntaxException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import io.socket.client.IO;
import io.socket.client.Manager;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.engineio.client.Transport;
import io.socket.engineio.client.transports.Polling;
import io.socket.engineio.client.transports.WebSocket;

@EApplication
public class UssdApplication extends Application {
    public static final String CHANNEL_ID = "autoStartServiceChannel";
    public static final String CHANNEL_NAME = "Auto Start Service Channel";
    private Socket mSocket;
    protected SharedPreferences preferences;
    @Override
    public void onCreate() {
        super.onCreate();
        createNotificationChannel();
        try {
            preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            getClientId();
            IO.Options opts = new IO.Options();
            opts.reconnection=true;
            opts.reconnectionDelay=0;
            opts.transports = new String[] { WebSocket.NAME};
            mSocket = IO.socket(Constants.RTC_SERVER_URL, opts);
            mSocket.io().on(Manager.EVENT_TRANSPORT, onTransport);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public Socket getSocket() {
        return mSocket;
    }

    private Emitter.Listener onTransport = args -> {
        Transport transport = (Transport) args[0];
        transport.on(Transport.EVENT_REQUEST_HEADERS, args12 -> {
            Map<String, List<String>> headers = (Map<String, List<String>>) args12[0];
            headers.put("io", Collections.singletonList(getClientId()));
        }).on(Transport.EVENT_RESPONSE_HEADERS, args1 -> {
        });
    };

    private String getClientId() {
        String clientId = preferences.getString("sessionId", StringUtils.EMPTY);
        if (StringUtils.isBlank(clientId)) {
            clientId = UUID.randomUUID().toString();
            preferences.edit().putString("sessionId", clientId).apply();
        }
        return clientId;
    }
}