package com.paygarde.ussd.dto;
import com.paygarde.ussd.model.ProcessStatus;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class PaymentProcess {
    private String id;
    private String ussdPattern;
    private String msinsdn;
    private int amount;
    private  Boolean isNew= Boolean.FALSE;
    private ProcessStatus status;
    private LocalDateTime currentStatusDate;
    private LocalDateTime endAt;
    private LocalDateTime startedAt;
    private List<String> logs = new ArrayList<>();
    private double afterBalance;
    public String getId() {
        return id;
    }

    public PaymentProcess setId(String id) {
        this.id = id;
        return this;
    }

    public LocalDateTime getEndAt() {
        return endAt;
    }

    public PaymentProcess setEndAt(LocalDateTime endAt) {
        this.endAt = endAt;
        return this;
    }

    public double getAfterBalance() {
        return afterBalance;
    }

    public PaymentProcess setAfterBalance(double afterBalance) {
        this.afterBalance = afterBalance;
        return this;
    }

    public LocalDateTime getStartedAt() {
        return startedAt;
    }

    public PaymentProcess setStartedAt(LocalDateTime startedAt) {
        this.startedAt = startedAt;
        return this;
    }

    public Boolean getNew() {
        return isNew;
    }

    public PaymentProcess setNew(Boolean aNew) {
        isNew = aNew;
        return this;
    }

    public List<String> getLogs() {
        return logs==null?new ArrayList<>():logs;
    }

    public PaymentProcess setLogs(List<String> logs) {
        this.logs = logs;
        return this;
    }

    public ProcessStatus getStatus() {
        return status;
    }

    public PaymentProcess setStatus(ProcessStatus status) {
        this.status = status;
        return this;
    }

    public LocalDateTime getCurrentStatusDate() {
        return currentStatusDate;
    }

    public PaymentProcess setCurrentStatusDate(LocalDateTime currentStatusDate) {
        this.currentStatusDate = currentStatusDate;
        return this;
    }

    public String getUssdPattern() {
        return ussdPattern;
    }

    public PaymentProcess setUssdPattern(String ussdPattern) {
        this.ussdPattern = ussdPattern;
        return this;
    }

    public String getMsinsdn() {
        return msinsdn;
    }

    public PaymentProcess setMsinsdn(String msinsdn) {
        this.msinsdn = msinsdn;
        return this;
    }

    public int getAmount() {
        return amount;
    }

    public PaymentProcess setAmount(int amount) {
        this.amount = amount;
        return this;
    }
}
