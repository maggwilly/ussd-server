package com.paygarde.ussd.fragment;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.paygarde.ussd.dto.PaymentProcess;

import java.util.List;

public class ProcessListViewModel extends ViewModel {
    private final MutableLiveData<List<PaymentProcess>> updates = new MutableLiveData<>();
    private final MutableLiveData<List<PaymentProcess>> news = new MutableLiveData<>();

    public void setUpdates(List<PaymentProcess> processData) {
        updates.setValue(processData);
    }
    public LiveData<List<PaymentProcess>> getUpdates() {
        return updates;
    }

    public void setNews(List<PaymentProcess> processData) {
        news.setValue(processData);
    }
    public LiveData<List<PaymentProcess>> getNews() {
        return news;
    }
}