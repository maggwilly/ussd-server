package com.paygarde.ussd.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paygarde.ussd.R;
import com.paygarde.ussd.UssdApplication;
import com.paygarde.ussd.ViewPagerAdapter;
import com.paygarde.ussd.dto.PaymentProcess;
import com.paygarde.ussd.service.RTCService;
import com.paygarde.ussd.service.SocketEmitter;
import com.paygarde.ussd.service.UssdService;

import org.androidannotations.annotations.App;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.EReceiver;
import org.apache.commons.lang3.BooleanUtils;

import java.text.DateFormat;
import java.util.Arrays;
import java.util.Collections;

import io.socket.client.Socket;


@EFragment
public class MainFragment extends Fragment{

    private CurrentOperationViewModel operationViewModel;
    private ViewPagerAdapter viewPagerAdapter;
    private ProcessListViewModel processListViewModel;
    public ViewPager viewPager;
    public TabLayout tabLayout;
    private View view;
    final Gson gson = new GsonBuilder().setDateFormat(DateFormat.FULL, DateFormat.DEFAULT).create();

    public MainFragment() {}

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_main, container, false);

        viewPagerAdapter = new ViewPagerAdapter(requireActivity().getSupportFragmentManager());
        // add the fragments
        viewPagerAdapter.add(new CurrentOperationFragment_(), "Current process");
        viewPagerAdapter.add(new ProcessListFragment_(), "Payment's logs");
        viewPagerAdapter.add(new SettingsFragment_(), "Client settings");
        viewPager = view.findViewById(R.id.viewpager);
        viewPager.setAdapter(viewPagerAdapter);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = view.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);
        operationViewModel = new ViewModelProvider(requireActivity()).get(CurrentOperationViewModel.class);
        processListViewModel = new ViewModelProvider(requireActivity()).get(ProcessListViewModel.class);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(receiver, new IntentFilter(RTCService.NOTIFICATION));
    }

    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String string = bundle.getString(RTCService.DATA);
                int resultCode = bundle.getInt(RTCService.RESULT);
                if (resultCode == Activity.RESULT_OK) {
                    String type = bundle.getString(RTCService.TYPE);
                    if(RTCService.TYPE_DATA.equals(type)) {
                        PaymentProcess paymentProcess = gson.fromJson(string,PaymentProcess.class);
                        operationViewModel.setCurrent(paymentProcess);
                        if (BooleanUtils.isTrue(paymentProcess.getNew()))
                            processListViewModel.setNews(Collections.singletonList(paymentProcess));
                        else
                            processListViewModel.setUpdates(Collections.singletonList(paymentProcess));
                    }else
                    Toast.makeText(getActivity(), string,Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getActivity(), string, Toast.LENGTH_LONG).show();
                }
            }
        }
    };
}