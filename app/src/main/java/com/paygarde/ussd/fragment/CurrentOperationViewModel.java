package com.paygarde.ussd.fragment;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.paygarde.ussd.dto.PaymentProcess;

public class CurrentOperationViewModel extends ViewModel {
    private final MutableLiveData<PaymentProcess> current = new MutableLiveData<>();
    public void setCurrent(PaymentProcess processData) {
        current.setValue(processData);
    }
    public LiveData<PaymentProcess> getCurrent() {
        return current;
    }
}