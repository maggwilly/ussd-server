package com.paygarde.ussd.fragment;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.paygarde.ussd.R;
import com.paygarde.ussd.model.ProcessData;
import com.paygarde.ussd.model.ProcessStatus;

import org.apache.commons.lang3.math.NumberUtils;

import java.util.List;
public class ProcessRecyclerViewAdapter extends RecyclerView.Adapter<ProcessRecyclerViewAdapter.ViewHolder> {
    private final List<ProcessData> mValues;

    public ProcessRecyclerViewAdapter(List<ProcessData> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_process, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        String title=String.format("%s | %s | %s XAF",mValues.get(position).getCurrentStatusDate(),mValues.get(position).getMsinsdn(),mValues.get(position).getAmount());
        holder.mTitleView.setText(title);
        holder.mStatusView.setText(holder.mItem.getStatus().name());
        holder.mBalanceView.setText(String.format("Balance after transaction: %s XAF",holder.mItem.getAfterBalance()));
        if(ProcessStatus.SUCCESS.equals(holder.mItem.getStatus()))
            holder.mBalanceView.setVisibility(View.VISIBLE);
        else
            holder.mBalanceView.setVisibility(View.GONE);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mTitleView;
        public final TextView mStatusView;
        public final TextView mBalanceView;
        public ProcessData mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitleView = view.findViewById(R.id.item_title);
            mStatusView = view.findViewById(R.id.status);
            mBalanceView = view.findViewById(R.id.balance);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mStatusView.getText() + "'";
        }
    }
}