package com.paygarde.ussd.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.preference.PreferenceFragmentCompat;

import com.paygarde.ussd.R;
import com.paygarde.ussd.model.ClientDetail;
import com.paygarde.ussd.service.SocketEmitter;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EFragment;

@EFragment
public class SettingsFragment extends PreferenceFragmentCompat  implements SharedPreferences.OnSharedPreferenceChangeListener{
    @Bean
    SocketEmitter socketEmitter;

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey);
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        ClientDetail data = new ClientDetail(key, sharedPreferences.getAll().get(key));
        socketEmitter.emit("clientDetailEvent", data);

    }
}