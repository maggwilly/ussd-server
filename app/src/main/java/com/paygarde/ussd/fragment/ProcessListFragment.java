package com.paygarde.ussd.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.preference.PreferenceManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.paygarde.ussd.Constants;
import com.paygarde.ussd.R;
import com.paygarde.ussd.dto.PaymentProcess;
import com.paygarde.ussd.model.ProcessData;
import com.paygarde.ussd.model.ProcessStatus;

import org.androidannotations.annotations.EFragment;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.OkHttpClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@EFragment
public class ProcessListFragment extends Fragment {
    public static final int PAGE_SIZE = 20;
    private SharedPreferences preferences;
    private RecyclerView recyclerView;
    private ProcessRecyclerViewAdapter processRecyclerViewAdapter;
    private ProcessListViewModel processListViewModel;
    private List<ProcessData> processDataList= new ArrayList<>();
    private int currentPage=0;
    private boolean isLoading;
    private boolean isDone;

    public ProcessListFragment() { }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);
        preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        processListViewModel = new ViewModelProvider(requireActivity()).get(ProcessListViewModel.class);
        processListViewModel.getUpdates().observe(getViewLifecycleOwner(),paymentProcessList -> {
            Map<String, ProcessStatus> map = paymentProcessList.stream().collect(Collectors.toMap(PaymentProcess::getId, PaymentProcess::getStatus));
            processDataList.forEach(processData -> {
                processData.setStatus(map.getOrDefault(processData.getId(),processData.getStatus()));
            });
            processRecyclerViewAdapter.notifyDataSetChanged();
        });
        processListViewModel.getNews().observe(getViewLifecycleOwner(),paymentProcessList -> {
            processDataList.addAll(0,convertAll(paymentProcessList));
            processRecyclerViewAdapter.notifyDataSetChanged();
        });
        initAdapter(view);
        initScrollListener();
        return view;
    }

    private Collection<? extends ProcessData> convertAll(Collection<PaymentProcess> paymentProcessList) {
        return paymentProcessList.stream().map(paymentProcess -> {
            ProcessData processData = getProcessData( paymentProcess);
            return processData;
        }).collect(Collectors.toCollection(ArrayList::new));
    }

    private ProcessData getProcessData( PaymentProcess paymentProcess) {
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        ProcessData processData = new ProcessData();
        processData.setStatus(paymentProcess.getStatus())
                .setMsinsdn(paymentProcess.getMsinsdn())
                .setId(paymentProcess.getId())
                .setCurrentStatusDate(df.format(new Date()))
                .setAmount(paymentProcess.getAmount())
                .setLogs(paymentProcess.getLogs());
        return processData;
    }

    private void initAdapter(View view) {
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            processRecyclerViewAdapter = new ProcessRecyclerViewAdapter(processDataList);
            recyclerView.setAdapter(processRecyclerViewAdapter);
            new LoadProcess().execute();
        }
    }

    private void initScrollListener() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (!isLoading) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == processDataList.size() - 1) {
                        loadMore();
                    }
                }
            }
        });

    }
    private void loadMore() {
        if(isDone)
            return;
          new LoadProcess().execute();
    }


    private class LoadProcess extends AsyncTask <Object, Void, ResponseEntity<ProcessData[]> > {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            isLoading = true;
        }

        @Override
        protected ResponseEntity<ProcessData[]> doInBackground(Object... params) {
            String clientId = preferences.getString("sessionId", StringUtils.EMPTY);
            OkHttpClientHttpRequestFactory requestFactory = new OkHttpClientHttpRequestFactory();
            RestTemplate template = new RestTemplate(requestFactory);
            return template.getForEntity(String.format(Constants.HTTP_SERVER_URL, clientId, currentPage,PAGE_SIZE), ProcessData[].class);
        }

        @Override
        protected void onPostExecute(ResponseEntity<ProcessData[]> responseEntity) {
            super.onPostExecute(responseEntity);
            if (Arrays.asList(201, 200, 204).contains(responseEntity.getStatusCode().value())) {
                if (ArrayUtils.isEmpty(responseEntity.getBody())) {
                    isDone = true;
                    return;
                }
                processDataList.addAll(Arrays.asList(responseEntity.getBody()));
                processRecyclerViewAdapter.notifyDataSetChanged();
                currentPage++;
                isLoading = false;
            }
        }
    }
}