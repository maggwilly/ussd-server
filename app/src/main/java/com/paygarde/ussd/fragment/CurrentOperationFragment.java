package com.paygarde.ussd.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.paygarde.ussd.R;

import org.androidannotations.annotations.EFragment;

@EFragment
public class CurrentOperationFragment extends Fragment {
    static final String TAG = "CurrentOperationFragment";

    private CurrentOperationViewModel mViewModel;
    private    View rootView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.current_operation_fragment, container, false);
        return rootView;
    }
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(requireActivity()).get(CurrentOperationViewModel.class);
        mViewModel.getCurrent().observe(getViewLifecycleOwner(), item -> {
            TextView currentName = rootView.findViewById(R.id.noProcess);
            LinearLayout linearLayoutCompat= rootView.findViewById(R.id.current_process);
            if(item==null) {
                currentName.setVisibility(View.VISIBLE);
                linearLayoutCompat.setVisibility(View.GONE);
            }else {
                currentName.setVisibility(View.GONE);
                linearLayoutCompat.setVisibility(View.VISIBLE);
                TextView msisdn = rootView.findViewById(R.id.process_msisdn);
                msisdn.setText(item.getMsinsdn());
                TextView amount = rootView.findViewById(R.id.process_amount);
                amount.setText(String.format("%s XAF",item.getAmount()));
                TextView ussdcode = rootView.findViewById(R.id.process_ussd);
                ussdcode.setText(item.getUssdPattern());
                TextView ussdStatus= rootView.findViewById(R.id.process_status);
                ussdStatus.setText(item.getStatus()!=null?item.getStatus().name(): "");
                ArrayAdapter<String> itemsAdapter = new ArrayAdapter<>(requireActivity(), android.R.layout.simple_list_item_1, item.getLogs());
                ListView listView = rootView.findViewById(R.id.list_view);
                listView.setAdapter(itemsAdapter);
            }
        });
    }

}